import React from 'react';
import './GetCountry.css';


const GetCountry = (props) => {
    return (
        props.name.map((name, index) => {
            return (
                <div key={index} className="GetCountry"  >
                    <p className="CntryName" onClick={() => props.clicked(name.name)}>{name.name}</p>
                </div>
            )
        })
    );
};

export default GetCountry;