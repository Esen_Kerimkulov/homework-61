import React, { Component } from 'react';
import axios from 'axios';

import './Country.css';
import GetCountry from '../GetCountry/GetCountry';
import CountryPost from "../CountryPost/CountryPost";


class Country extends Component {
    state = {
        name: [],
        selectedPostName: null
    };

    componentDidMount() {
        axios.get('https://restcountries.eu/rest/v2/all?fields=name;alpha3Code').then(response => {
            this.setState({name: response.data})
        })


}
    getInfoCountry = (name) => {
        console.log(name);
        axios.get(`https://restcountries.eu/rest/v2/name/${name}`).then(response => {
            const country = response.data[0];
            Promise.all(country.borders.map(border => axios.get(`https://restcountries.eu/rest/v2/alpha/${border}` )))
                .then(responseBorder => {
                    country.borders = responseBorder.map(border => border.data.name);
                    this.setState({selectedPostName: country
                    })
                })
        })
    };


    render() {
        return (
            <div>
            <section className="Countr">
                <GetCountry
                    name={this.state.name}
                    clicked={this.getInfoCountry}
                />
            </section>
            <section>
                <CountryPost info={this.state.selectedPostName}/>
            </section>
            </div>
        );
    }
}

export default Country;