import React, {Component} from 'react';
import './CountryPost.css';

class CountryPost extends Component {
    render() {
        if(!this.props.info) return null;
        return (
            <div className="CountryPost">
                <p>Country name: {this.props.info.name}</p>
                <p>Capital: {this.props.info.capital}</p>
                <p>Population: {this.props.info.population} people</p>
                <p>Border With: {this.props.info.borders}</p>

                <div>
                    <img src={this.props.info.flag} alt=""/>
                </div>
            </div>

        );
    }
}

export default CountryPost;