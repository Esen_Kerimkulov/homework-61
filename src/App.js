import React, { Component } from 'react';
import Country from './components/Country/Country';

class App extends Component {
  render() {
    return <Country />;
  }
}

export default App;
